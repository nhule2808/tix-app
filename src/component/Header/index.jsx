import React from "react";
import "./header.scss";
function Header(props) {
  return (
    <header className="wp-header">
      <div className="header container-fluid">
        <a className="back_page ">
          <i className="fas fa-chevron-left" />
        </a>
        <a className="back_page" />
        <div className="header__logo">
          <a>
            <img src="./images/web-logo.png" className="img-fluid" alt="logo" />
          </a>
        </div>
        <div className="header__wp-list-group">
          <ul className="list-unstyled list-item mb-0">
            <li className="item">
              <a>Lịch chiếu</a>
            </li>
            <li className="item">
              <a>Cụm rạp</a>
            </li>
            <li className="item">
              <a>Tin tức</a>
            </li>
            <li className="item">
              <a>Ứng dụng</a>
            </li>
          </ul>
        </div>
        <div className="header__wp-content">
          <div className="info-user">
            <img src="./images/anh-dai-dien.png" alt="username" />
            <span className="username ml-1">Võ Khắc Tín</span>
            <div className="info-user__logout">Đăng xuất</div>
          </div>
          <div className="selected-location">
            <div className="dropdown">
              <div
                className="btn btn-white dropdown-toggle dropdown-button"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="false"
                aria-expanded="false"
              >
                <i className="fa fa-map-marker-alt" />
                <p className="mb-0 mr-5">Hà Nội</p>
                <i className="fas fa-angle-down" />
              </div>
              <ul
                className="dropdown-menu selectScroll"
                aria-labelledby="dropdownMenuButton"
              >
                <li>
                  <a className="dropdown-item">Hồ Chí Minh</a>
                </li>
                <li>
                  <a className="dropdown-item">Hà Nội</a>
                </li>
                <li>
                  <a className="dropdown-item">Đà Nẵng</a>
                </li>
                <li>
                  <a className="dropdown-item">Hải Phòng</a>
                </li>
                <li>
                  <a className="dropdown-item">Biên Hòa</a>
                </li>
                <li>
                  <a className="dropdown-item">Bình Dương</a>
                </li>
                <li>
                  <a className="dropdown-item">Phan Thiết</a>
                </li>
                <li>
                  <a className="dropdown-item">Hạ Long</a>
                </li>
                <li>
                  <a className="dropdown-item">Quy Nhơn</a>
                </li>
                <li>
                  <a className="dropdown-item">Vũng tàu</a>
                </li>
                <li>
                  <a className="dropdown-item">Buôn Ma Thuộc</a>
                </li>
                <li>
                  <a className="dropdown-item">Bạc Liêu</a>
                </li>
                <li>
                  <a className="dropdown-item">Long An</a>
                </li>
                <li>
                  <a className="dropdown-item">Đồng Hới</a>
                </li>
                <li>
                  <a className="dropdown-item">Đồng Tháp</a>
                </li>
                <li>
                  <a className="dropdown-item">Hưng Yên</a>
                </li>
                <li>
                  <a className="dropdown-item">Mỹ Tho</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div
          className="header__menu_dropdown "
          // onclick="showMenu()"
        >
          <i className="fas fa-bars " />
        </div>
      </div>
      <div className="sidebarMenu " id="sidebarDrop">
        <ul className="list-unstyled list-dropdown">
          <li className="mb-5 mt-3">
            <div className="wp-info-drop">
              <a>
                <img
                  src="./images/anh-dai-dien.png"
                  className="img-fluid"
                  alt="anh dai dien"
                />
              </a>
              <span>Võ Khắc Tín</span>
            </div>
            <i
              className="fas fa-angle-right"
              id="icon-menudrop"
              // onclick="hideMenu()"
            />
          </li>
          <li className="mb-5">
            <a>Lịch chiếu</a>
          </li>
          <li className="mb-5">
            <a>Cụm rạp</a>
          </li>
          <li className="mb-5">
            <a>Tin tức</a>
          </li>
          <li className="mb-5">
            <a>Ứng dụng</a>
          </li>
          <li className="mb-5">
            <a>Hà Nội</a>
          </li>
          <li className="mb-5">
            <a>Đăng xuất</a>
          </li>
        </ul>
      </div>
      <div
        className="overlay"
        id="overlayId"
        // onclick="hideMenuByOverLay()"
      />
    </header>
  );
}

export default Header;
