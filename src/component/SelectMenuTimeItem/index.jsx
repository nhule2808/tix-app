import React from "react";
import format from "date-format";
import { useDispatch } from "react-redux";
import { showTimeOnMenu } from "../../redux/actions/getTimeShowTime";
function SelectMenuTimeItem(props) {
  let dispatch = useDispatch();
  let { item } = props;
  // console.log(item);
  function handleShowTime() {
    dispatch(showTimeOnMenu(format("hh:mm", new Date(item))));
  }
  return (
    <li
      className="dropdown-item dropdown-item--listTheater"
      onClick={handleShowTime}
    >
      {format("hh:mm", new Date(item))}
    </li>
  );
}

export default SelectMenuTimeItem;
