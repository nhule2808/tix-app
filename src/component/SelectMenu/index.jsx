import React, { useEffect, useState } from "react";
import SelectMenuFilm from "../SelectMenuFilm";
import { GetListMovieTheaterByIdRequest } from "../../redux/actions/getListMovieTheatersById";
import "./selectMenu.scss";
import { useDispatch } from "react-redux";
import SelectMenuMovieTheater from "../SelectMenuMovieTheater";
import SelectMenuDate from "../SelectMenuDate";
import SelectMenuTime from "../SelectMenuTime";
function SelectMenu(props) {
  let dispatch = useDispatch();
  let [idFilm, setIdFilm] = useState(-1);
  function getIdFilm(id) {
    setIdFilm(id);
  }
  useEffect(() => {
    dispatch(GetListMovieTheaterByIdRequest(idFilm));
  }, [idFilm]);
  return (
    <section className="selectMenu">
      <SelectMenuFilm getIdFilm={getIdFilm} />
      <SelectMenuMovieTheater />

      <SelectMenuDate />
      <SelectMenuTime />
      <button className="btn_bookingTicketNow">Mua vé ngay</button>
    </section>
  );
}

export default SelectMenu;
