import { combineReducers } from "redux";
import SelectMenuReducer from "./SelectMenuReducer";
import Movie from "./MoiveReducer";
import CinemaHome from "./CinemaHome";
const rootReducer = combineReducers({ SelectMenuReducer, Movie, CinemaHome });
export { rootReducer };
