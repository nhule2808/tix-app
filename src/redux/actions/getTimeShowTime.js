function getTimeShowTime(dataDate) {
  return {
    type: "GET_TIME_SHOW_TIME",
    payload: dataDate,
  };
}
function showTimeOnMenu(data) {
  return {
    type: "SHOW_TIME_ON_MENU",
    payload: data,
  };
}
export { showTimeOnMenu };
export { getTimeShowTime };
