import React from "react";
import Banner from "../../component/Banner";
import Header from "../../component/Header";
import SelectMenu from "../../component/SelectMenu";
import ListTabFilm from "../../component/ListTabFilm";
import "./home.scss";
import CinemaSysHome from "../../component/CinemaSysHome";
import Application from "../../component/Application";
import Footer from "../../component/Footer";

function Home(props) {
  return (
    <div className="wrapper" id="wp-content">
      <Header />
      <Banner />
      <SelectMenu />
      <ListTabFilm />
      <CinemaSysHome />
      <Application />
      <Footer />
    </div>
  );
}
export default Home;
